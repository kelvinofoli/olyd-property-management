import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AllPropertiesComponent} from './all-properties/all-properties.component'
import {PropertyComponent} from './property/property.component'
import {HomeComponent} from './home/home.component'

const routes: Routes = [

  {path: '', component: HomeComponent},
  {path: 'all', component: AllPropertiesComponent},
  {path: 'property/:id',component: PropertyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
